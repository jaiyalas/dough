
## build

To compile Dough, you need [Stack](https://docs.haskellstack.org/en/stable/README/) on your computer. If you don't know how to start, please follow [How to install](https://docs.haskellstack.org/en/stable/README/#how-to-install) to install.

Once you have stack ready, then:

```bash
git clone git@gitlab.com:9chapters/dough.git
cd dough
stack build
stack install
```

## usage

Please go to [teznames](https://gitlab.com/tezos-southeast-asia/teznames) to see a realistic use case of Dough.

### the bash scripting

There are two scriptings

```bash
./comp.sh
./dough.sh
```

Both of them are just handy tool for typing less when using stack. They will be removed after this pre-stage development.

### configuration

To run Dough, you need one config file for overall Dough configuration. It's required to be written in terms of haskell's record structure as following example:

```haskell
ClientConfig
   { nodeAddress = "node1.lax.tezos.org.sg"
   , who         = "dotblack"
   , burnCap     = 10
   , ptclTNS     = "9chsTNS-0.0.1"
   , ptclTNRS    = "9chsTNRS-0.0.1"
   , cTNRSAddr   = "KT1P4fir3MtexzztWbybNxonCv28Fu6hwmGA"
   }
```

The first three fields are more general setting:

+ setting the node address,
+ the role who Dough is acting as; and,
+ how many burn-cap you want for deploying or transfering.

The latter three fields are for [teznames](https://gitlab.com/tezos-southeast-asia/teznames) only:

+ the protocol of teznames
+ address of tezname record contract

### data preparation

There are two kinds of interaction we can do to/with a contract: to originate and to transfer. We call them as _deploy_ a contract and _invoke_ a contract in Dough. Both of them requires some data or information from user. To do so, user only needs to write another config-liked file for listing all necessary information.

One of the key feature of Dough is, this information is not necessary to be all of what a contract required, because Dough can do some propressing for users! As an example, to _renew_ a _tezname_, user only need to provide a file like:

```haskell
InvokeArgs
   { ivkTransfer = 0
   , ivkInfo     = TNS_Renew
   }
```

### commands

Dough provides 5 basic functions as in 5 different commands.

#### `dough show`

To show all the local TZ accounts (whom tezos-clinet can actually get secrete key)

```bash
> dough show -r
```

+ [optional] `-c <path_to_config_file>` config file
+ [optional] `-r` right alignment

If the `-c` argument waas not specified, Dough will look for a file named `dough.config` located in user's home directory.

#### `dough inquiry`

To show the storage of a given contract

```bash
> dough inquiry -c <path_to_file> -n <contract_alias>
```

+ [optional] `-c <path_to_config_file>` config file
+ [must have] `-n <contract_alias>` alias of targeting contract

#### `dough data`

One way to use Dough is to use it as a prepared-for-tezos-client Michelson data preparator. Namely, a one-lined Michelson data generator.

`dough data` can translate a given Haskell data into one-lined Michelson format which is required for deploying or invoking a contract manually.

```bash
> dough data -c <path_to_config_file> -d <path_to_input_file>  -o <path_to_output_file>
```

+ [optional] `-c <path_to_config_file>` config file
+ [must have] `-d <path_to_input_file>` input data file as in Haskell record type
+ [optional] `-o <path_to_output_file>` path to output file

#### `dough deploy`

To originate or deploy a contract:

```bash
> dough deploy -c <path_to_config_file> -d <path_to_input_file> -i <path_to_.tz_file> -n <contract_alias>
```

+ [optional] `-c <path_to_config_file>`
+ [must have] `-d <path_to_input_file>` input data file as in Haskell record type
+ [must have] `-i <path_to_.tz_file>` the path to source contract (.tz)
+ [must have] `-n <contract_alias>` the alias of TzName contract

#### `dough invoke`

```bash
> dough invoke -c <path_to_config_file> -n <contract_alias> -i <path_to_input_file>
```

+ [optional] `-c <path_to_config_file>`
+ [must have] `-n <contract_alias>` the alias of TzName contract
+ [must have] `-i <path_to_input_file>` input data file as in Haskell record type
