{-# LANGUAGE OverloadedStrings #-}
--
module Tezos.Client.Connect where
--
import Shelly
--
import Tezos.Client.Account
--
import Control.Monad
import Control.Monad.Reader
--
import Data.Maybe (catMaybes)
import Data.Monoid
import qualified Data.Text as T
--
data ClientConfig = ClientConfig
   { nodeAddress :: T.Text
   , ptclTNS     :: T.Text
   , ptclTNRS    :: T.Text
   , who         :: T.Text
   , cTNRSAddr   :: T.Text
   , burnCap     :: Int
   } deriving (Show, Read)
--
type ClientEnv a = Reader ClientConfig a
--
runTzClient :: [T.Text] -> ClientEnv (Sh T.Text)
-- ^ wrapping @tezos-client@ as in "Shelly" for given parameters.
runTzClient ts = return $ run "tezos-client" ts
--
specNode :: ClientEnv [T.Text]
-- ^ specifying the IP address or host of tezos node.
specNode = do
   env <- ask
   return ["-A", nodeAddress env]
--
getStorage :: ClientEnv [T.Text]
getStorage = return ["get", "script", "storage", "for"]
--
getKeys :: T.Text -> ClientEnv (Sh T.Text)
getKeys name = do
   toNode <- specNode
   runTzClient $ toNode <> ["show", "address", name, "-S"]
--
showKnownAddr :: ClientEnv (Sh T.Text)
showKnownAddr = do
   toNode <- specNode
   runTzClient $ toNode <> ["list", "known", "addresses"]
--
getAllAlias :: ClientEnv (Sh [T.Text])
getAllAlias = do
   addrs <- showKnownAddr
   return $ liftM (catMaybes . map (getAccSK . T.words) . T.lines) addrs
--
tzTransfer :: ClientConfig
           -> T.Text
           -> T.Text
           -> T.Text
           -> T.Text
           -> T.Text
           -> IO  T.Text
tzTransfer cf tf from to arg burncap =
   shelly $ print_stdout False $ (flip runReader) cf $ do
      nodeInfo <- specNode
      runTzClient $ nodeInfo
         <> ["transfer"  , tf]
         <> ["from"      , from]
         <> ["to"        , to]
         <> ["--arg"     , arg]
         <> ["--burn-cap", burncap]
--
tzSign :: ClientConfig -> T.Text -> T.Text -> IO T.Text
tzSign cf mQuestion mName =
   shelly $ print_stdout False $ (flip runReader) cf $ do
      nodeInfo <- specNode
      runTzClient $ nodeInfo <> ["sign", "bytes", mQuestion, "for", mName]
