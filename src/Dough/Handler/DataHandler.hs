{-# LANGUAGE OverloadedStrings #-}
--
module Dough.Handler.DataHandler where
--
import Shelly
--
import System.IO
--
import Tezos.Data
import Tezos.Client
--
import Dough.Args.OptArg
import Dough.Utils.TzNameData
--
import qualified Data.Text as T
--
dataHandler :: ClientConfig -> DataOpt -> IO ()
dataHandler cf opt = do
   raw <- loadData (inFile opt)
   expr <- tzNameData2Expr cf raw
   let ctx = michelsonDataRender expr
   case (outFile opt) of
      Nothing ->
         shelly $ print_stdout False $ do
            echo ctx
      (Just outfile) -> whiteToFile outfile (T.unpack ctx)
--
whiteToFile :: String -> String -> IO ()
whiteToFile fname content = do
   h <- openFile fname WriteMode
   hPutStr h content
   hFlush h
   hClose h
   return ()
--
