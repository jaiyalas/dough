{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE ExtendedDefaultRules #-}
-- {-# OPTIONS_GHC -fno-warn-type-defaults #-}
--
module Main where
--
import Dough
import Tezos.Client
-- import Tezos.Data
--
import Shelly
--
import Options.Applicative (execParser)
--
-- import System.IO
import System.Directory (getHomeDirectory)
--
-- import Control.Monad
-- import Control.Monad.Reader
--
-- import Data.Monoid
--
-- import qualified Data.Maybe as Maybe
import qualified Data.Text as T
import Text.Read (readMaybe)
-- default (T.Text)
--
main :: IO ()
main = do
   -- putStrLn "[\ESC[32m\STXExecuting \ESC[4;33m\STXDough\ESC[32m\STX ..\ESC[0m\STX]"
   x <- execParser opts
   homePath <- getHomeDirectory
   let cfPath = if ((getCFPath x) == "")
                  then (homePath ++ "/dough.config")
                  else (getCFPath x)
   configContent <- readFile cfPath
   let cf' = (readMaybe configContent) :: Maybe ClientConfig
   case (cf', x) of
      (Just cf, ShowCmd    cmd) -> showHandler    cf cmd
      (Just cf, InquiryCmd cmd) -> inquiryHandler cf cmd
      (Just cf, DataCmd    cmd) -> dataHandler    cf cmd
      (Just cf, InvokeCmd  cmd) -> invokeHandler  cf cmd
      (Just cf, DeployCmd  cmd) -> deployHandler  cf cmd
      (Nothing, _) -> putStrLn $
         "\ESC[1;37;41m\STX" ++ "[Cannot detect or parse config file: "++
            (show cfPath)++"]" ++ "\ESC[0m\STX"
   -- putStrLn "[\ESC[32m\STXTerminating \ESC[4;33m\STXDough\ESC[32m\STX ..\ESC[0m\STX]"
--
