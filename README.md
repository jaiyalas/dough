# Dough

<img src="img/dough_logo.png" alt="drawing" width="350"/>

<img src="img/alert.png" alt="drawing" width="600"/>

Dough makes your daily life easier by letting you interact with tezos contract easier. It provides a _command line tool_ and _haskell library_ for interacting with tezows-client and even more other functions that can makes your tezos daily life simpler!

Want to know more? Just read [our documents](https://dough.readthedocs.io/)!
